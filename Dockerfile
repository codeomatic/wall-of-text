FROM python:3.9-alpine

RUN apk add --no-cache \
    build-base \
    bash \
    linux-headers


# Copy python requirements file
COPY requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

# Remove pip cache. We are not going to need it anymore
RUN rm -r /root/.cache

ENV PYTHONUNBUFFERED 1


# Add our application files
COPY wot /code
WORKDIR /code

EXPOSE 5000

CMD ["python3", "main.py"]

