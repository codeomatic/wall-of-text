import os
import time

from flask import Flask, request, jsonify, send_from_directory
from flask_cors import CORS
from flask_socketio import SocketIO, join_room, leave_room, send, emit, rooms


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret'
CORS(app)
ws = SocketIO(app, cors_allowed_origins='*')

all_rooms = {}

@ws.on('join')
def on_join(room):
    join_room(room)
    all_rooms.setdefault(room, 0)
    all_rooms[room] += 1

@ws.on('leave')
def on_leave(room):
    leave_room(room)
    all_rooms[room] -= 1

@ws.on('message')
def on_message(msg):
    # broadcast to everyone in the same room on the 'message' channel
    my_rooms = rooms() # get all rooms of this user
    my_rooms.remove(request.sid) # remove personal room
    emit(
        'message', 
        {
            'message': msg, 
            'timestamp': int(time.time())
        },
        room=my_rooms[-1],
        json=True
    )


@app.route('/rooms')
def list_rooms():
    return jsonify(list(all_rooms.keys()))


@app.route('/<path:path>')
def svelte_client(path):
    return send_from_directory('public/', path)


#@app.route('/')
#def index():
#    return send_from_directory('public', 'index.html')

@app.route('/')
def index():
    return 'It works! I promiss!!'

if __name__ == '__main__':
    ws.run(
        app,
        host='0.0.0.0', # listen to outside requests 
        port=int(os.getenv('PORT', '5000')),
        debug=True,
    )
